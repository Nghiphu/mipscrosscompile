/* Generated automatically. */
static const char configuration_arguments[] = "../configure --prefix=/opt/cross/mips-linux-musl --target=mips-linux-musl --enable-languages=c --disable-libmudflap --disable-libsanitizer --disable-nls --disable-multilib --with-multilib-list=";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "llsc", "llsc" } };
